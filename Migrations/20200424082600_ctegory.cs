﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ApiGestionVin.Migrations
{
    public partial class ctegory : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "CatergorieId",
                table: "Products",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Domaine",
                table: "Products",
                nullable: true);

            migrationBuilder.AddColumn<float>(
                name: "Prix",
                table: "Products",
                nullable: false,
                defaultValue: 0f);

            migrationBuilder.CreateTable(
                name: "Category",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Designation = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Category", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Products_CatergorieId",
                table: "Products",
                column: "CatergorieId");

            migrationBuilder.AddForeignKey(
                name: "FK_Products_Category_CatergorieId",
                table: "Products",
                column: "CatergorieId",
                principalTable: "Category",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Products_Category_CatergorieId",
                table: "Products");

            migrationBuilder.DropTable(
                name: "Category");

            migrationBuilder.DropIndex(
                name: "IX_Products_CatergorieId",
                table: "Products");

            migrationBuilder.DropColumn(
                name: "CatergorieId",
                table: "Products");

            migrationBuilder.DropColumn(
                name: "Domaine",
                table: "Products");

            migrationBuilder.DropColumn(
                name: "Prix",
                table: "Products");
        }
    }
}
