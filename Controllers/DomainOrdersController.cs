﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ApiGestionVin.Models;

namespace ApiGestionVin.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DomainOrdersController : ControllerBase
    {
        private readonly ApiContext _context;

        public DomainOrdersController(ApiContext context)
        {
            _context = context;
        }

        // GET: api/DomainOrders
        [HttpGet]
        public async Task<ActionResult<IEnumerable<DomainOrder>>> GetDomainOrder()
        {
            return await _context.DomainOrder.ToListAsync();
        }

        // GET: api/DomainOrders/5
        [HttpGet("{id}")]
        public async Task<ActionResult<DomainOrder>> GetDomainOrder(int id)
        {
            var domainOrder = await _context.DomainOrder.FindAsync(id);

            if (domainOrder == null)
            {
                return NotFound();
            }

            return domainOrder;
        }

        // PUT: api/DomainOrders/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutDomainOrder(int id, DomainOrder domainOrder)
        {
            if (id != domainOrder.Id)
            {
                return BadRequest();
            }

            _context.Entry(domainOrder).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!DomainOrderExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/DomainOrders
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<DomainOrder>> PostDomainOrder(DomainOrder domainOrder)
        {
            _context.DomainOrder.Add(domainOrder);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetDomainOrder", new { id = domainOrder.Id }, domainOrder);
        }

        // DELETE: api/DomainOrders/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<DomainOrder>> DeleteDomainOrder(int id)
        {
            var domainOrder = await _context.DomainOrder.FindAsync(id);
            if (domainOrder == null)
            {
                return NotFound();
            }

            _context.DomainOrder.Remove(domainOrder);
            await _context.SaveChangesAsync();

            return domainOrder;
        }

        private bool DomainOrderExists(int id)
        {
            return _context.DomainOrder.Any(e => e.Id == id);
        }
    }
}
