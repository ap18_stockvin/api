﻿using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using Microsoft.EntityFrameworkCore.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApiGestionVin.Models
{
    public class SeedData
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            var context = (ApiContext) serviceProvider.GetService(typeof(ApiContext));

            if (!context.Products.Any())
            {

                context.Categories.AddRange(
                    new Category { Id = 1, Designation = "Rouge" },
                    new Category { Id = 2, Designation = "Blanc" },
                    new Category { Id = 3, Designation = "Rosé" },
                    new Category { Id = 4, Designation = "Digestifs" },
                    new Category { Id = 5, Designation = "Pétillants" }
                    );
                context.Domains.AddRange(
                    new Domain { Id = 1, Designation = "Tariquet" },
                    new Domain { Id = 2, Designation = "Pelleheaut" },
                    new Domain { Id = 3, Designation = "Joy" },
                    new Domain { Id = 4, Designation = "Vignoble Fontan" },
                    new Domain { Id = 5, Designation = "Uby" },
                    new Domain { Id = 6, Designation = "Canard Duchene" }
                    );
                context.Products.AddRange(
                    new Product { Id = 1, Domain = context.Domains.Find(1), Seuil = 50, Designation = "Premiere Grives", Categorie = context.Categories.Find(2), Annee=2019, Prix = 15.90F, Quantite = 12},
                    new Product { Id = 2, Domain = context.Domains.Find(2), Seuil = 100, Designation = "AMPELOMERYX BLANC", Categorie = context.Categories.Find(2), Annee=2018, Prix = 8.50F, Quantite = 54},
                    new Product { Id = 3, Domain = context.Domains.Find(3), Seuil = 200, Designation = "Ode à la Joie", Categorie = context.Categories.Find(2), Annee=2018, Prix = 8.10F, Quantite = 34},
                    new Product { Id = 4, Domain = context.Domains.Find(4), Seuil = 20, Designation = "L'eau de là", Categorie = context.Categories.Find(4), Annee=2013, Prix = 30.90F, Quantite = 73},
                    new Product { Id = 5, Domain = context.Domains.Find(5), Seuil = 50, Designation = "COLOMBARD-SAUVIGNON", Categorie = context.Categories.Find(2), Annee=2019, Prix = 5.30F, Quantite = 10},
                    new Product { Id = 6, Domain = context.Domains.Find(5), Seuil = 150, Designation = "Byo", Categorie = context.Categories.Find(1), Annee=2019, Prix = 7.50F, Quantite = 60}
                    );
                
        
                context.Customer.AddRange(
                    new Customer { Prenom = "Antonio", Nom = "Vivaldi"},
                    new Customer { Prenom = "Wolfgang Amadeus", Nom = "Mozart"},
                    new Customer { Prenom = "Robert", Nom = "Schumann"},
                    new Customer { Prenom = "Jean-Sébastien", Nom = "Bach"},
                    new Customer { Prenom = "Maurice", Nom = "Ravel"},
                    new Customer { Prenom = "Sergei", Nom = "Rachmaninoff"}
                    );

                context.CustomerOrder.AddRange(
                    new CustomerOrder { CustomerId = 1, ProductId = 6, Date = new DateTime(), Quantity = 20, IsRegister = false },
                    new CustomerOrder { CustomerId = 2, ProductId = 5, Date = new DateTime(), Quantity = 10, IsRegister = false }
                    );

                context.SaveChanges();
            }
        }
    }
}
