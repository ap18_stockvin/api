﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ApiGestionVin.Models
{
    public class Domain
    {
        [Key]
        public int Id { get; set; }

        public string Designation { get; set; }
        public ICollection<Product> Products { get; set; }

    }
}
