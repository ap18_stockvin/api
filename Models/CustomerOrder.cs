﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ApiGestionVin.Models
{
    public class CustomerOrder
    {
        [Key]
        public int Id { get; set; }

        public DateTime Date { get; set; }

        public int? ProductId { get; set; }
        public Product Product { get; set; }

        public int? CustomerId { get; set; }
        public Customer Customer { get; set; }

        public int Quantity { get; set; }

        public bool IsRegister { get; set; }
    }
}
