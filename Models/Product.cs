﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ApiGestionVin.Models
{
    public class Product
    {
        [Key]
        public int Id { get; set; }

        public string Designation { get; set; }

        public int? Annee { get; set; }

        public float Prix { get; set; }

        public int Quantite { get; set; }

        public int Seuil { get; set; }

        [Required]
        public int? CategorieId { get; set; }
        public Category Categorie { get; set; }

        public int? DomainId { get; set; }
        public Domain Domain { get; set; }
    }
}
