﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApiGestionVin.Models;
using System.Diagnostics;

namespace ApiGestionVin.Models
{
    public class ApiContext : DbContext
    {

        public ApiContext(DbContextOptions options) : base(options)
        {
            try
            {
                Database.EnsureCreated();
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
        }
        
        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            options.UseMySql("server=localhost;port=3306;userid=root;password=;database=api_vin;");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Product>()
                .HasOne<Category>(p => p.Categorie)
                .WithMany(c => c.Products)
                .HasForeignKey(p => p.CategorieId);
            ;            
        }

        public DbSet<Product> Products { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Customer> Customer { get; set; }
        public DbSet<CustomerOrder> CustomerOrder { get; set; }
        public DbSet<DomainOrder> DomainOrder { get; set; }
        public DbSet<Domain> Domains { get; set; }

    }
}
